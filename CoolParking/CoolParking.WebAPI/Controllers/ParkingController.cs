﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using CoolParking.BL.Interfaces;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    public class ParkingController : ControllerBase,IDisposable
    {
        readonly IParkingService _parking;
        private bool disposed = false;

        public ParkingController(IParkingService parkingService)
        {
            _parking = parkingService;
        }
        // GET: api/parking/balance
        [Route("api/[controller]/[action]")]
        [HttpGet]
        public ActionResult<decimal> Balance()
        {
            return Ok(_parking.GetBalance());
        }

        // GET: api/parking/capacity
        [Route("api/[controller]/[action]")]
        [HttpGet]
        public ActionResult<int> Capacity()
        {
            return Ok(_parking.GetCapacity());
        }

        // GET: api/parking/capacity
        [Route("api/[controller]/[action]")]
        [HttpGet]
        public ActionResult<int> FreePlaces()
        {
            return Ok(_parking.GetFreePlaces());
        }

        // GET: api/vehicles
        [Route("api/[action]")]
        [HttpGet]
        public ActionResult<string> Vehicles()
        {
            return Ok(JsonConvert.SerializeObject(_parking.GetVehicles()));
        }
        // GET: api/vehicles/AA-1111-AA
        [Route("api/[action]/{id}")]
        [HttpGet("{id}")]
        public ActionResult<string> Vehicles(string id)
        {
            string message;
            try
            {
                message = JsonConvert.SerializeObject(new VehicleModel(_parking.Find(id)));
            }
            catch (ArgumentOutOfRangeException)
            {
                return BadRequest();
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            return Ok(message);
        }
        // GET: api/vehicles/AA-1111-AA
        [Route("api/[action]")]
        [HttpPost]
        [ActionName("vehicles")]
        public ActionResult AddVehicle([FromBody] VehicleModel json)
        {
            try
            {
                _parking.AddVehicle(new Vehicle(json.id, (VehicleType)json.vehicleType, json.balance));
            }
            catch
            {
                return BadRequest();
            }
            return Created($"api/vehicles/{json.id}",_parking.Find(json.id));
        }
        // GET: api/vehicles/AA-1111-AA
        [Route("api/[action]/{id}")]
        [HttpDelete]
        [ActionName("vehicles")]
        public ActionResult DeleteVehicle(string id)
        {
            try
            {
                _parking.RemoveVehicle(id);
            }
            catch (ArgumentOutOfRangeException)
            {
                return BadRequest();
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            return NoContent();
        }
        [Route("api/transactions/last")]
        [HttpGet]
        public ActionResult<string> Transactions()
        {
            return Ok(JsonConvert.SerializeObject(_parking.GetLastParkingTransactions()));
        }
        [Route("api/transactions/all")]
        [HttpGet]
        public ActionResult<string> TransactionsAll()
        {
            try
            {
                return Ok(_parking.ReadFromLog());
            }
            catch
            {
                return NotFound();
            }
        }
        [Route("api/transactions/topUpVehicle")]
        [HttpPut]
        [ActionName("vehicles")]
        public ActionResult TopUpVehicle([FromBody]TopUpModel topUp)
        {
            try
            {
                _parking.TopUpVehicle(topUp.id, topUp.sum);
                return Ok(JsonConvert.SerializeObject(new VehicleModel(_parking.Find(topUp.id))));
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch
            {
                return BadRequest();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    _parking.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                //CloseHandle(handle);
                // handle = IntPtr.Zero;

                // Note disposing has been done.
                disposed = true;
            }
        }
        public class VehicleModel
        {
            public string id { get; set; }
            public int vehicleType { get; set; }
            public decimal balance { get; set; }
            public VehicleModel(Vehicle vehicle)
            {
                id = vehicle.Id;
                vehicleType = (int)vehicle.VehicleType;
                balance = vehicle.Balance;
            }

            public VehicleModel()
            {
            }
        }
        public class TopUpModel
        {
            public string id { get; set; }
            public decimal sum { get; set; }
        }
    }
}
