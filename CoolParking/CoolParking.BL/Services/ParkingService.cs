﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private bool disposed = false;
        ITimerService _withdrawTimer;
        public Parking _parking;
        ITimerService _logTimer;
        ILogService _logService;
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetInstance();

            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += new ElapsedEventHandler(Withdraw);
            _logTimer = logTimer;
            _logTimer.Elapsed += new ElapsedEventHandler(Log);
            _logService = logService;
            _withdrawTimer.Start();
            _logTimer.Start();
        }
        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }
        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.ParkingPlaces.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.ParkingPlaces);
        }
        public void AddVehicle(Vehicle vehicle)
        {
            _parking.Add(vehicle);
        }
        public Vehicle Find(string vehicle)
        {
           return _parking.Find(vehicle);
        }
        public void RemoveVehicle(string vehicleId)
        {
            _parking.Remove(vehicleId);
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            _parking.TopUpVehicle(vehicleId, sum);
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.currentTransactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }
        public string ReadCurrentTransactions()
        {
            StringBuilder Message = new StringBuilder("No Cars");
            foreach (var transaction in _parking.currentTransactions)
            {
                Message.AppendLine($"Date:{DateTime.Now};CarID:{transaction.vehicleId};Payment:{transaction.sum}");
            }
            return Message.ToString();
        }
        public decimal ReadCurrentProfit()
        {
            return _parking.currentTransactions.ToArray().Sum(tr => tr.sum);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    _parking.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                //CloseHandle(handle);
                // handle = IntPtr.Zero;

                // Note disposing has been done.
                disposed = true;
            }
        }
        private void Withdraw(object source, ElapsedEventArgs e)
        {
            _parking.Withdraw();
        }
        private void Log(object source, ElapsedEventArgs e)
        {
            StringBuilder Message = new StringBuilder("No Cars");
            foreach (var transaction in _parking.currentTransactions)
            {
                Message.AppendLine($"Date:{DateTime.Now};CarID:{transaction.vehicleId};Payment:{transaction.sum}");
            }
            _parking.currentTransactions.Clear();
            _logService.Write(Message.ToString());
        }
    }
}