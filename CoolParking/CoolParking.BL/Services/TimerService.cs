﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public TimerService(double interval)
        {
            Interval= interval;
            timer = new System.Timers.Timer();
        }
        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }

        public void Start()
        {
            timer.Elapsed += Elapsed;
            timer.Interval=Interval;
            timer.AutoReset = true;
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }

        public void Dispose()
        {
            timer.Dispose();
        }

    }
}