﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string path = Settings.logFilePath)
        {
            LogPath = path;
            try
            {
                if (!File.Exists(LogPath))
                {
                    using (FileStream fs = File.Create(LogPath))
                    {
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public string LogPath { get; }
        public void Write(string message)
        {
            using (StreamWriter log = new StreamWriter(LogPath,true, System.Text.Encoding.Default))
            {
                log.WriteLine(message);
            }

        }
        public string Read()
        {
            string log = "empty";
            if (File.Exists(LogPath))
            {
                log = System.IO.File.ReadAllText(LogPath);
            }
            else
            {
                throw new InvalidOperationException();
            }
            return log;
        }
    }
}