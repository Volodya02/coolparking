﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
       public static string GenerateRandomRegistrationPlateNumber() 
        {
            Random rnd = new Random();
            char randomChar = (char)rnd.Next('a', 'z');
            String Plate = "";
            Plate += (char)rnd.Next('A', 'Z');
            Plate += (char)rnd.Next('A', 'Z');
            Plate += "-";
            Plate += rnd.Next(0, 9);
            Plate += rnd.Next(0, 9);
            Plate += rnd.Next(0, 9);
            Plate += rnd.Next(0, 9);
            Plate += "-";
            Plate += (char)rnd.Next('A', 'Z');
            Plate += (char)rnd.Next('A', 'Z');

            return Plate;
        }
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; set; }
        public Vehicle(string Id, VehicleType VehicleType, decimal Balance)
        {
            Regex regex = new Regex(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
            if (regex.IsMatch(Id) && Balance >= 0 && VehicleType.IsDefined(typeof(VehicleType), VehicleType))
            {
                this.Id = Id;
                this.VehicleType = VehicleType;
                this.Balance = Balance;
            }
            else
            {
                throw new System.ArgumentException("ID формату ХХ-YYYY-XX (де X - будь-яка літера англійського алфавіту у верхньому регістрі, а Y - будь-яка цифра, наприклад DV-2345-KJ)", "ID");
            }
        }
        public void TopUP(decimal sum)
        {

            if (sum>=0)
            {
                this.Balance += sum;
            }
            else
            {
                throw new System.ArgumentException("this is not withdraw method", "ID");
            }
        }
    }
}