﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartBalance = 0;
        public const string logFilePath = @"Transactions.log";
        public static int MaxCapacity = 10;
        public static int DefaultCountTime = 5000;
        public static int DefaultLogTime = 60000;
        public static Dictionary<VehicleType, decimal> Tarrifs = new Dictionary<VehicleType, decimal>
    {
        {VehicleType.PassengerCar, 2M},
        {VehicleType.Truck, 5M},
        {VehicleType.Bus, 3.5M},
        {VehicleType.Motorcycle, 1M}
    };
        public static decimal Penalty = 2.5M;
    }
}