﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private bool disposed = false;
        public decimal Balance { get; private set; }
        public List<Vehicle> ParkingPlaces { get; private set; }
        public int Capacity { get; private set; }
        public List<TransactionInfo> currentTransactions { get; private set; }
        private Parking(decimal balance, int capacity)
        {
            this.Balance = balance;
            this.Capacity = capacity;
            this.ParkingPlaces = new List<Vehicle>();
            this.currentTransactions = new List<TransactionInfo>();
        }
        private static volatile Lazy<Parking> _instance;
        public static Parking GetInstance()
        {
            if (_instance == null)
                _instance = new Lazy<Parking>(() => new Parking(Settings.StartBalance, Settings.MaxCapacity));

            return _instance.Value;
        }
        public void Add(Vehicle vehicle)
        {
            int id = this.ParkingPlaces.FindIndex(x => x.Id == vehicle.Id);
            if (id != -1)
            {
                throw new System.ArgumentException("", "ID");
            }
            else if (ParkingPlaces.Count == Capacity)
            {
                throw new System.InvalidOperationException("Max Capacity");
            }
            else
            {
                ParkingPlaces.Add(vehicle);
            }
        }
        public void Remove(String vehicleId)
        {
            int id = this.ParkingPlaces.FindIndex(x => x.Id == vehicleId);
            Regex regex = new Regex(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
            if (!regex.IsMatch(vehicleId) || vehicleId == "")
            {
                throw new System.ArgumentOutOfRangeException();
            }
            else if (id == -1)
            {
                throw new System.ArgumentException("", "ID");

            }
            else if (ParkingPlaces[id].Balance < 0)
            {
                throw new System.InvalidOperationException("Top Up First");
            }
            else
            {
                ParkingPlaces.RemoveAll(x => x.Id == vehicleId);
            }
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            int id = this.ParkingPlaces.FindIndex(x => x.Id == vehicleId);

            Regex regex = new Regex(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
            if (!regex.IsMatch(vehicleId) || vehicleId == "")
            {
                throw new System.InvalidOperationException();
            }
            else if (id != -1 && sum >= 0)
            {
                this.ParkingPlaces[id].TopUP(sum);
            }
            else
            {
                throw new System.ArgumentException("", "ID");
            }
        }
        public Vehicle Find(string vehicleId)
        {

            int id = this.ParkingPlaces.FindIndex(x => x.Id == vehicleId);

            Regex regex = new Regex(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
            if (!regex.IsMatch(vehicleId))
            {
                throw new System.ArgumentOutOfRangeException();
            }
            else if (id != -1)
            {
                return ParkingPlaces.First(x => x.Id == vehicleId);
            }
            else
            {
                throw new System.ArgumentException("", "ID");
            }
        }
        public void Withdraw()
        {
            foreach (var car in ParkingPlaces)
            {
                decimal payment = Settings.Tarrifs[car.VehicleType] * (car.Balance > 0 ? 1 : Settings.Penalty);
                car.Balance -= payment;
                currentTransactions.Add(new TransactionInfo(DateTime.Now, car.Id, payment));
                this.Balance += payment;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _instance = null;
                }
                disposed = true;
            }
        }
    }
}