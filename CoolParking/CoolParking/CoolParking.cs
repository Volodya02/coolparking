﻿using System;
using System.Net.Http;
using System.Text;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
namespace CoolParking
{
    class CoolParking
    {
        private static HttpClient _client;
        static void Main(string[] args)
        {
            _client = new HttpClient();
            string[] options = new string[] {
                "Вивести на екран поточний баланс Паркінгу.",//0
                "Вивести на екран суму зароблених коштів за поточний період (до запису у лог).",//1
                "Вивести на екран кількість вільних місць на паркуванні (вільно X з Y).",//2
                "Вивести на екран усі Транзакції Паркінгу за поточний період (до запису у лог).",//3
                "Вивести на екран історію Транзакцій (зчитавши дані з файлу Transactions.log).",//4
                "Вивести на екран список Тр. засобів , що знаходяться на Паркінгу.",//5
                "Поставити Транспортний засіб на Паркінг.",//6
                "Забрати Транспортний засіб з Паркінгу.",//7
                "Поповнити баланс конкретного Тр. засобу.",//8
                "Кількість місць для паркування",/*9*/
            "Знайти машину",/*10*/};
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            int flag = 0;
            ConsoleKeyInfo key;
            bool working = true;
            while (working)
            {
                //show options 
                for (int i = 0; i < options.Length; i++)
                {
                    if (i == flag)
                        Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(new string(' ', (Console.WindowWidth - options[i].Length) / 2)); // centring
                    Console.WriteLine(options[i]);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                //choose option
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.Enter:
                        Action(flag);
                        break;
                    case ConsoleKey.Escape:
                        working = false;
                        break;
                    case ConsoleKey.UpArrow:
                        flag = (flag + options.Length - 1) % options.Length;
                        break;
                    case ConsoleKey.DownArrow:
                        flag = (flag + options.Length + 1) % options.Length;
                        break;
                    default:
                        Console.WriteLine("use arrow keys to navigate commands;use Esc to exit ;use Enter to choose command");
                        break;
                }
                Console.Clear();
            }
            async void Action(int flag)
            {
                Console.Clear();
                Console.WriteLine("Use arrows to choose option;Use Enter to select it;use Esc to exit app");
                switch (flag)
                {
                    case 0:
                        var parkingbalance = _client.GetStringAsync("https://localhost:44321/api/parking/balance").Result;
                        Console.WriteLine("Current Balance = " + parkingbalance);
                        break;
                    case 1:
                        Console.WriteLine("not implemented in webapi");
                        break;
                    case 2:
                        var freeplaces = _client.GetStringAsync("https://localhost:44321/api/parking/freePlaces").Result;
                        Console.WriteLine("Amount of Empty Places " + freeplaces);
                        break;
                    case 3:
                        var transaction = _client.GetStringAsync("https://localhost:44321/api/transactions/last").Result;
                        Console.WriteLine("Current transactions are :" + transaction);
                        break;
                    case 4:
                        var log = _client.GetStringAsync("https://localhost:44321/api/transactions/all").Result;
                        Console.WriteLine("Log :" + log);
                        break;
                    case 5:
                        Console.WriteLine("Cars :");
                        Console.WriteLine(_client.GetStringAsync("https://localhost:44321/api/vehicles").Result);
                        break;
                    case 6:
                        string Id = "";
                        Console.WriteLine("Adding new Vechile, please enter CarId:");
                        Id = Console.ReadLine();
                        VehicleType type;
                        Console.WriteLine("Adding new Vechile, please enter CarType(PassengerCar,Truck,Bus,Motorcycle):");
                        type = (VehicleType)Enum.Parse(typeof(VehicleType), Console.ReadLine(), true);
                        decimal balance;
                        Console.WriteLine("Adding new Vechile, please enter Balance:");
                        balance = Convert.ToDecimal(Console.ReadLine());
                        string vehiclejson = $"{{  \"id\": \"{Id}\", \"vehicleType\": {(int)type}, \"balance\": {balance}}}";
                        var stringContent = new StringContent(vehiclejson, Encoding.Default, "application/json");
                        var answer = _client.PostAsync("https://localhost:44321/api/vehicles", stringContent).Result.StatusCode;
                        Console.WriteLine(vehiclejson);
                        Console.WriteLine(answer);
                        break;
                    case 7:
                        Console.WriteLine("Remove Vechile, please enter CarId:");
                        var delete = _client.DeleteAsync($"https://localhost:44321/api/vehicles/{Console.ReadLine()}").Result.StatusCode;
                        Console.WriteLine(delete);
                        break;
                    case 8:
                        string CarId = "";
                        Console.WriteLine("TopUp Vechile, please enter CarId:");
                        CarId = Console.ReadLine();
                        decimal sum;
                        Console.WriteLine("TopUp Vechile, please enter Sum:");
                        sum = Convert.ToDecimal(Console.ReadLine());
                        string json = $"{{ \"id\": \"{CarId}\", \"Sum\": {sum} }}";
                        var stringContent1 = new StringContent(json, Encoding.Default, "application/json");
                        var topUp = _client.PutAsync("https://localhost:44321/api/transactions/topUpVehicle", stringContent1).Result;
                        var code = topUp.Content.ReadAsByteArrayAsync();
                        Console.WriteLine(Encoding.Default.GetString(await code));
                        break;
                    case 9:
                        var capacity = _client.GetStringAsync("https://localhost:44321/api/parking/capacity").Result;
                        Console.WriteLine("Capacity = " + capacity);
                        break;
                    case 10:
                        string id = "";
                        Console.WriteLine("Find Vechile, please enter CarId:");
                        id = Console.ReadLine();
                        var path = $"https://localhost:44321/api/vehicles/" + id;
                        var searchedcar = _client.GetStringAsync(path).Result;
                        Console.WriteLine("car = " + searchedcar);
                        break;
                    default:
                        Action(flag);
                        break;
                }
                Console.WriteLine("Press Any Key to return");
                Console.ReadKey();
            }
        }
    }
}